import React from 'react';
import { Link } from 'react-router-dom';

function Sidemenu() {

    return (
        <div>
            {/*  <!-- left sidebar -->
            <!-- ============================================================== --> */}
            <div className="nav-left-sidebar sidebar-dark">
                <div className="menu-list">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <a className="d-xl-none d-lg-none" href="#">Dashboard</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav flex-column">
                                <li className="nav-divider">
                                    Menu
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link active" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-1" aria-controls="submenu-1"><i
                                            className="fa fa-fw fa-user-circle"></i>Dashboard <span
                                                className="badge badge-success">6</span></a>
                                    <div id="submenu-1" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item">
                                                <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                                    data-target="#submenu-1-2" aria-controls="submenu-1-2">E-Commerce</a>
                                                <div id="submenu-1-2" className="collapse submenu" >
                                                    <ul className="nav flex-column">

                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/ecommerce_product'} > product </Link>
                                                        </li>
                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/ecommerce_product_single'} > single </Link>
                                                        </li>
                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/ecommerce_product_checkout'} > checkout </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/dashboard_finance'} > finance</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/dashboard_sales'} > sales </Link>
                                            </li>

                                            <li className="nav-item">
                                                <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                                    data-target="#submenu-1-1" aria-controls="submenu-1-1">Infulencer</a>
                                                <div id="submenu-1-1" className="collapse submenu" >
                                                    <ul className="nav flex-column" className="nav-link">
                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/dashboard_influencer'} > influencer</Link>
                                                        </li>
                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/influencer_finder'} > finder </Link>
                                                        </li>
                                                        <li className="nav-item" className="nav-link">
                                                            <Link to={'/influencer_profile'} > profile </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-2" aria-controls="submenu-2"><i
                                            className="fa fa-fw fa-rocket"></i>UI Elements</a>
                                    <div id="submenu-2" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/cards'} > Cards</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/general'} > General </Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/cards'} > Carousel</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/listgroup'} > Listgroup</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/typography'} > Typography</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/accordions'} > Accordions</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/tabs'} > Tabs</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-3" aria-controls="submenu-3"><i
                                            className="fas fa-fw fa-chart-pie"></i>Chart</a>
                                    <div id="submenu-3" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/charts_c3'} >  C3</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/chart_Chartist'} > Chartist</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/chart_charts'} > Charts</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/chart_morris'} > Morris</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/chart_sparkline'} > Sparkline</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/chart_gauge'} > Gauge</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-4" aria-controls="submenu-4"><i
                                            className="fab fa-fw fa-wpforms"></i>Forms</a>
                                    <div id="submenu-4" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Form_element'} > Form Elements</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Form_validation'} >Parsely
                                                    Validations </Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Multiselect'} > Multiselect</Link>
                                            </li>
                                            {/* <li className="nav-item">
                                                <a className="nav-link" href="pages/datepicker.html">Date Picker</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="pages/bootstrap-select.html">Bootstrap Select</a>
                                            </li> */}
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-5" aria-controls="submenu-5"><i
                                            className="fas fa-fw fa-table"></i>Tables</a>
                                    <div id="submenu-5" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/general_table'} > General_table</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/data_table'} > Data_table</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-divider">
                                    Features
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-6" aria-controls="submenu-6"><i className="fas fa-fw fa-file"></i>
                                        Pages </a>
                                    <div id="submenu-6" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Blank_page'} > Blank page</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Blanl_page_header'} > Blank page header</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/login'} > Login </Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Notfound_page'} > 404 page</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/sign_up'} > Sign up</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/forgot/password'} > Forgot password</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">

                                                <Link to={'/pricing'} >Pricing</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/timeline'} > Timeline</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/calender'} > Calender</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">

                                                <Link to={'/sortable_nestable_table'} > Sortable/Neastable Table </Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/widgets'} > Widgets</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Media_objects'} > Media Objects</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/cropper_image'} > Cropper</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/color_picker'} > Color Picker</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <div id="submenu-7" className="collapse submenu">
                                        <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                            data-target="#submenu-7" aria-controls="submenu-7"><i
                                                className="fas fa-fw fa-inbox"></i>Apps <span
                                                    className="badge badge-secondary">New</span></a>
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Inbox'} > Inbox</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Email_details'} > Email details</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Email_compose'} > Email compose</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Message_chat'} > Message chat</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-8" aria-controls="submenu-8"><i
                                            className="fas fa-fw fa-columns"></i>Icons</a>
                                    <div id="submenu-8" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Icon_fontawesome'} >fontawesome</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Icon_material'} > material</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Icon_simple_lineicon'} > simple lineicon</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Icon_themify'} > themify</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/icon_flag'} > flag</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Icon_weather'} > weather</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-9" aria-controls="submenu-9"><i
                                            className="fas fa-fw fa-map-marker-alt"></i>Maps</a>
                                    <div id="submenu-9" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Map_google'} > Google</Link>
                                            </li>
                                            <li className="nav-item" className="nav-link">
                                                <Link to={'/Map_vector'} > Vector</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                        data-target="#submenu-10" aria-controls="submenu-10"><i
                                            className="fas fa-f fa-folder"></i>Menu Level</a>
                                    <div id="submenu-10" className="collapse submenu" >
                                        <ul className="nav flex-column">
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Level 1</a>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                                    data-target="#submenu-11" aria-controls="submenu-11">Level 2</a>
                                                <div id="submenu-11" className="collapse submenu" >
                                                    <ul className="nav flex-column">
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="#">Level 1</a>
                                                        </li>
                                                        <li className="nav-item">
                                                            <a className="nav-link" href="#">Level 2</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li className="nav-item">
                                                <a className="nav-link" href="#">Level 3</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    )
}

export default Sidemenu;