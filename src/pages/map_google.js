import React from 'react';
function Map_google() {
    return (

        <div className="dashboard-wrapper">
            <div className="container-fluid dashboard-content">
                {/* ============================================================== */}
                {/* pageheader */}
                {/* ============================================================== */}
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="page-header">
                            <h2 className="pageheader-title">Google Map </h2>
                            <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div className="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                        <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Maps</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">Google Map</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/* ============================================================== */}
                {/* end pageheader */}
                {/* ============================================================== */}
                <div className="row">
                    {/* ============================================================== */}
                    {/* basic map */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Basic Map</h5>
                            <div className="card-body">
                                <div id="map" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end basic map */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        {/* ============================================================== */}
                        {/* map events */}
                        {/* ============================================================== */}
                        <div className="card">
                            <h5 className="card-header">Map Events</h5>
                            <div className="card-body">
                                <div id="map_1" className="gmaps" />
                            </div>
                        </div>
                        {/* ============================================================== */}
                        {/* end map events */}
                        {/* ============================================================== */}
                    </div>
                </div>
                <div className="row">
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        {/* ============================================================== */}
                        {/* markers */}
                        {/* ============================================================== */}
                        <div className="card">
                            <h5 className="card-header">Markers</h5>
                            <div className="card-body">
                                <div id="map_2" className="gmaps" />
                            </div>
                        </div>
                        {/* ============================================================== */}
                        {/* end markers */}
                        {/* ============================================================== */}
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        {/* ============================================================== */}
                        {/* polylines */}
                        {/* ============================================================== */}
                        <div className="card">
                            <h5 className="card-header">Polylines</h5>
                            <div className="card-body">
                                <div id="map_3" className="gmaps" />
                            </div>
                        </div>
                        {/* ============================================================== */}
                        {/* end polylines */}
                        {/* ============================================================== */}
                    </div>
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/* polygons */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Polygons</h5>
                            <div className="card-body">
                                <div id="map_4" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* polygons */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Routes</h5>
                            <div className="card-body">
                                <div id="map_5" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* polygons */}
                    {/* ============================================================== */}
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/* routes advance */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Routes Advance</h5>
                            <div className="card-body">
                                <div id="map_6" className="gmaps" />
                                <a href="#" id="start_travel" className="btn btn-primary m-t-20">start</a>
                                <ul id="instructions">
                                </ul>
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end routes advance */}
                    {/* ============================================================== */}
                    {/* ============================================================== */}
                    {/*street advance */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Street View Panoramas</h5>
                            <div className="card-body">
                                <div id="panorama" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/*end street advance */}
                    {/* ============================================================== */}
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/* map types */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Map Types</h5>
                            <div className="card-body">
                                <div id="map_7" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/*end map advance */}
                    {/* ============================================================== */}
                    {/* ============================================================== */}
                    {/*fusion tables */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Fusion Tables layers</h5>
                            <div className="card-body">
                                <div id="map_8" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/*end fusion tables layers */}
                    {/* ============================================================== */}
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/*kml layers */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">KML layers</h5>
                            <div className="card-body">
                                <div id="map_9" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end kml layers */}
                    {/* ============================================================== */}
                    {/* ============================================================== */}
                    {/* geofences */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Geofences</h5>
                            <div className="card-body">
                                <div id="map_10" className="gmaps" />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end geofences */}
                    {/* ============================================================== */}
                </div>
            </div>
        </div>
    );
}
export default Map_google;