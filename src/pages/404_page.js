import React from 'react'

function NotFound_page() {
  return (
    <div>
      {/* wrapper  */}
      {/* ============================================================== */}
      <div className="bg-light text-center">
        <div className="container">
          <div className="row">
            <div className="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
              <div className="error-section">
                <img src="../public/assets/images/error-img.png" alt="" className="img-fluid" />
                <div className="error-section-content">
                  <h1 className="display-3">Page Not Found</h1>
                  <p> The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                  <a href="../index.html" className="btn btn-secondary btn-lg">Back to homepage</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default NotFound_page
