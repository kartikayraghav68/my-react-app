import React from 'react';

function Chart_morris() {
    return (
        <div>
            {/* <!-- wrapper  -->
        <!-- ============================================================== --> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Morris.js </h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Charts</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Morris.js</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== --> */}

                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!--area chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Area Chart</h5>
                                <div className="card-body">
                                    <div id="morris_area"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!--end area chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!--line chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Chart</h5>
                                <div className="card-body">
                                    <div id="morris_line"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!--end line chart  -->
                        <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!--bar chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Bar Chart</h5>
                                <div className="card-body">
                                    <div id="morris_bar"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!--end bar chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!--stacked chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Stacked Bars </h5>
                                <div className="card-body">
                                    <div id="morris_stacked"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!--end stacked chart  -->
                        <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!-- upadating chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Updating data </h5>
                                <div className="card-body">
                                    <div id="morris_udateing"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end upadating chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- donut chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Donut Chart </h5>
                                <div className="card-body">
                                    <div id="morris_donut"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end donut chart  -->
                        <!-- ============================================================== --> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Chart_morris;