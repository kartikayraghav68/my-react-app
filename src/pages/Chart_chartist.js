import React from 'react';

function Chart_chartist() {
    return (
        <div>
            {/* <!-- wrapper  -->
        <!-- ============================================================== --> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Chartist.js </h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Charts</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Chartist.js</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- simple line chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Simple Line Chart</h5>
                                <div className="card-body">
                                    <div className="ct-chart-line ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end simple line chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- holes in data chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Holes In Data</h5>
                                <div className="card-body">
                                    <div className="ct-chart-holes ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end holes in data chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- whole numbers chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Whole Numbers</h5>
                                <div className="card-body">
                                    <div className="ct-chart-wnumbers ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end whole numbers charts -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- line scatter chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Scatter Diagram With Responsive Settings</h5>
                                <div className="card-body">
                                    <div className="ct-chart-scatter ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end line scatter chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- line chart with area  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Chart With Area</h5>
                                <div className="card-body">
                                    <div className="ct-chart-area ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end line chart with area  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- bi polar chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Bi-polar Line Chart With Area Only</h5>
                                <div className="card-body">
                                    <div className="ct-chart-polar ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end bi polar chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!--stacked bar chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Stacked Bar Chart</h5>
                                <div className="card-body">
                                    <div className="ct-chart-scatter-bar ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!--end stacked bar chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!--multi line chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Multi Line Labels</h5>
                                <div className="card-body">
                                    <div className="ct-chart-multilines ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!--end multi line chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!--multi line lables chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Multi-line Labels</h5>
                                <div className="card-body">
                                    <div className="ct-chart-bipolar ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end multi line lables chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- peak circle using the draw  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Add Peak Circles Using The Draw Events</h5>
                                <div className="card-body">
                                    <div className="ct-chart-events ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end peak circle using the draw  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- advanced smill chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Advanced Smil Animations</h5>
                                <div className="card-body">
                                    <div className="ct-chart-animation ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end advanced smill chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- horizontal chart bar  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Horizontal Chart Bar</h5>
                                <div className="card-body">
                                    <div className="ct-chart-horizontal ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end horizontal chart bar  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- simple pie chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Simple Pie Chart</h5>
                                <div className="card-body">
                                    <div className="ct-chart-pie ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end simple pie chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- donut chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Donut Chart Using Fill Rather Than Stroke</h5>
                                <div className="card-body">
                                    <div className="ct-chart-donut ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end donut chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- animations donut chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Animating A Donut With Svg.animate</h5>
                                <div className="card-body">
                                    <div className="ct-chart-animated ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end animations donut chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Chart_chartist;