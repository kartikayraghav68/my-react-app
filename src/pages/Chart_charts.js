import React from 'react';

function Chart_charts() {
    return (
        <div>
            {/* <!-- wrapper  -->
        <!-- ============================================================== --> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Chart.js</h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Charts</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Chart.js</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- line chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_line"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end line chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- bar chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Bar Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_bar"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end bar chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- radar chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Radar Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_radar"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end radar chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- polar chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Polar Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_polar"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end polar chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                    <!-- pie chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Pie Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_pie"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end pie chart  -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- doughnut chart  -->
                    <!-- ============================================================== --> */}
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Doughnut Charts</h5>
                                <div className="card-body">
                                    <canvas id="chartjs_doughnut"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                    <!-- end doughnut chart  -->
                    <!-- ============================================================== --> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Chart_charts;