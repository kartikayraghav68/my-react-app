import React from 'react'
function Map_vector() {
    return (

        <div className="dashboard-wrapper">
            <div className="container-fluid dashboard-content">
                {/* ============================================================== */}
                {/* pageheader */}
                {/* ============================================================== */}
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="page-header">
                            <h2 className="pageheader-title">Vector Map </h2>
                            <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div className="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                        <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Maps</a></li>
                                        <li className="breadcrumb-item active" aria-current="page">Vector Map</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/* ============================================================== */}
                {/* end pageheader */}
                {/* ============================================================== */}
                <div className="row">
                    {/* ============================================================== */}
                    {/* world map */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">World Map</h5>
                            <div className="card-body">
                                <div id="world-map-markers" style={{ height: '450px' }} />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/*end world map */}
                    {/* ============================================================== */}
                    {/* ============================================================== */}
                    {/*india */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">India</h5>
                            <div className="card-body">
                                <div id="india" style={{ height: '450px' }} />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/*end india */}
                    {/* ============================================================== */}
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/*USA */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">USA</h5>
                            <div className="card-body">
                                <div id="usa" style={{ height: '450px' }} />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end USA */}
                    {/* ============================================================== */}
                    {/* ============================================================== */}
                    {/* Australia map */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Australia Map</h5>
                            <div className="card-body">
                                <div id="australia" style={{ height: '450px' }} />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* end Australia map */}
                    {/* ============================================================== */}
                </div>
                <div className="row">
                    {/* ============================================================== */}
                    {/* UK map */}
                    {/* ============================================================== */}
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div className="card">
                            <h5 className="card-header">Uk Map</h5>
                            <div className="card-body">
                                <div id="uk" style={{ height: '450px' }} />
                            </div>
                        </div>
                    </div>
                    {/* ============================================================== */}
                    {/* UK map */}
                    {/* ============================================================== */}
                </div>
            </div></div>
    );
}
export default Map_vector;