import React from 'react';

function Chart_gauge() {
    return (
        <div>
            {/* <!-- wrapper  -->
        <!-- ============================================================== --> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Gauge</h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Charts</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Gauge</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== --> */}

                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!-- gauge one  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Gauge#1</h5>
                                <div className="card-body">
                                    <canvas id="gauge1"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end gauge one  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- gauge two  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Gauge#2</h5>
                                <div className="card-body">
                                    <canvas id="gauge2"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end gauge two  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- gauge three  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Gauge#3</h5>
                                <div className="card-body">
                                    <canvas id="gauge3"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end gauge three  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- gauge four  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Gauge#4</h5>
                                <div className="card-body">
                                    <canvas id="gauge4"></canvas>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end gauge four  -->
                        <!-- ============================================================== --> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Chart_gauge;