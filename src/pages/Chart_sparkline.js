import React from 'react';

function Chart_sparkline() {
    return (
        <div>
            {/* <!-- wrapper  -->
        <!-- ============================================================== --> */}
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Sparkline </h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Charts</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Sparkline</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== --> */}

                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!-- bar chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Bar Chart</h5>
                                <div className="card-body">
                                    <div id="sparkline1" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end bar chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- line chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Chart</h5>
                                <div className="card-body">
                                    <div id="sparkline2" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end line chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- discreate chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Discrete</h5>
                                <div className="card-body">
                                    <div id="sparkline3" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end discreate chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- line chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Line Chart</h5>
                                <div className="card-body">
                                    <div id="sparkline4" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end line chart  -->
                        <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/* <!-- ============================================================== -->
                        <!-- line chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Compositebar Line Chart</h5>
                                <div className="card-body">
                                    <div id="compositebar" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end line chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- bullet chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Bullet Chart</h5>
                                <div className="card-body">
                                    <div id="sparkline5" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end bullet chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- pie chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">PieChart</h5>
                                <div className="card-body">
                                    <div id="sparkline6" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end pie chart  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- box chart  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-3 col-lg-6    col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Box Plot</h5>
                                <div className="card-body">
                                    <div id="sparkline7" className="spark-chart"></div>
                                    <div className="spark-chart-info">
                                        <h5 className="mb-0">Sales</h5>
                                        <p>70%</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end box chart  -->
                        <!-- ============================================================== --> */}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Chart_sparkline;