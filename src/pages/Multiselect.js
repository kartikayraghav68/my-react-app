import React from 'react';

function Multiselect() {
    return (
        <div>
            <div className="dashboard-wrapper">
                <div className="container-fluid  dashboard-content">
                    {/* <!-- ============================================================== -->
                    <!-- pageheader -->
                    <!-- ============================================================== --> */}
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="page-header">
                                <h2 className="pageheader-title">Multiselect </h2>
                                <p className="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div className="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol className="breadcrumb">
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Dashboard</a></li>
                                            <li className="breadcrumb-item"><a href="#" className="breadcrumb-link">Forms</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">Multiselect</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- ============================================================== -->
                    <!-- end pageheader -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- multiselects  -->
                    <!-- ============================================================== --> */}

                    <div className="row">
                        {/*   <!-- ============================================================== -->
                        <!-- boxed multiselect  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Boxed Multiselect</h5>
                                <div className="card-body">
                                    <select multiple="multiple" id="my-select" name="my-select[]">
                                        <option defaultValue='elem_1'>Elements 1</option>
                                        <option defaultValue='elem_2'>Elements 2</option>
                                        <option defaultValue='elem_3'>Elements 3</option>
                                        <option defaultValue='elem_4'>Elements 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end boxed multiselect  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- pre multiselectd options  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Pre-selected options</h5>
                                <div className="card-body">
                                    <select id='pre-selected-options' multiple='multiple'>
                                        <option defaultValue='elem_1' selected>Elements 1</option>
                                        <option defaultValue='elem_2'>Elements 2</option>
                                        <option defaultValue='elem_3'>Elements 3</option>
                                        <option defaultValue='elem_4' selected>Elements 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end pre multiselectd options  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- callbacks multiselectd  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Callbacks</h5>
                                <div className="card-body">
                                    <select id='callbacks' multiple='multiple'>
                                        <option defaultValue='elem_1'>Elements 1</option>
                                        <option defaultValue='elem_2'>Elements 2</option>
                                        <option defaultValue='elem_3'>Elements 3</option>
                                        <option defaultValue='elem_4'>Elements 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/*  <!-- ============================================================== -->
                        <!-- end callbacks multiselectd  -->
                        <!-- ============================================================== --> */}
                    </div>
                    <div className="row">
                        {/*  <!-- ============================================================== -->
                        <!-- keep over multiselectd  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Keep Over</h5>
                                <div className="card-body">
                                    <select id='keep-order' multiple='multiple'>
                                        <option defaultValue='elem_1'>Elements 1</option>
                                        <option defaultValue='elem_2'>Elements 2</option>
                                        <option defaultValue='elem_3'>Elements 3</option>
                                        <option defaultValue='elem_4'>Elements 4</option>
                                        <option defaultValue='elem_5'>Elements 5</option>
                                        <option defaultValue='elem_6'>Elements 6</option>
                                        <option defaultValue='elem_7'>Elements 7</option>
                                        <option defaultValue='elem_8'>Elements 8</option>
                                        <option defaultValue='elem_9'>Elements 9</option>
                                        <option defaultValue='elem_10'>Elements 10</option>
                                        <option defaultValue='elem_11'>Elements 11</option>
                                        <option defaultValue='elem_12'>Elements 12</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/*  <!-- ============================================================== -->
                        <!-- end keep over multiselectd  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- optgroup multiselectd  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Optgroup</h5>
                                <div className="card-body">
                                    <select id='optgroup' multiple='multiple'>
                                        <optgroup label='Friends'>
                                            <option defaultValue='1'>Yoda</option>
                                            <option defaultValue='2' selected>Obiwan</option>
                                        </optgroup>
                                        <optgroup label='Enemies'>
                                            <option defaultValue='3'>Palpatine</option>
                                            <option defaultValue='4' disabled>Darth Vader</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- end optgroup multiselectd  -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- disabled multiselectd  -->
                        <!-- ============================================================== --> */}
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="card">
                                <h5 className="card-header">Disabled attribute</h5>
                                <div className="card-body">
                                    <select id='disabled-attribute' disabled='disabled' multiple='multiple'>
                                        <option defaultValue='elem_1'>Elements 1</option>
                                        <option defaultValue='elem_2'>Elements 2</option>
                                        <option defaultValue='elem_3'>Elements 3</option>
                                        <option defaultValue='elem_4'>Elements 4</option>
                                        <option defaultValue='elem_5'>Elements 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {/* <!-- ============================================================== -->
                        <!-- disabled multiselectd  -->
                        <!-- ============================================================== --> */}
                    </div>

                </div>
            </div>
        </div>
    )
}
export default Multiselect;