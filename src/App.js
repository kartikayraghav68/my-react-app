import logo from './logo.svg';
import './App.css';
import Sidemenu from './components/SideMenu';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import Influencer_finder from './pages/Influencer_finder';
import Influencer_profile from './pages/Influencer_profile';
import Ecommerce_product from './pages/Ecommerce_product';
import Ecommerce_product_single from './pages/Ecommerce_product_single';
import Ecommerce_product_checkout from './pages/Ecommerce_product_checkout';
import Dashboard_sales from './pages/Dashboard_sales'
import Dashboard_finance from './pages/Dashboard_finance';
import Dashboard_influencer from './pages/Dashboard_influencer';
import Accordions from './pages/Accordions';
import Cards from './pages/Cards';
import Carousel from './pages/Carousel';
import General from './pages/General';
import Listgroup from './pages/Listgroup';
import Tabs from './pages/tabs';
import Typography from './pages/typography';
import Form_element from './pages/Form_element'
import Form_validation from './pages/Form_validation';
import Multiselect from './pages/Multiselect';
import General_table from './pages/general_table';
import Data_table from './pages/data_tables';
import Map_google from './pages/map_google';
import Map_vector from './pages/map_vector';
import Icon_flag from './pages/icon_flag';
import Icon_fontawesome from './pages/icon_fontawesome';
import Icon_material from './pages/icon_material';
import Icon_themify from './pages/icon_themify';
import Icon_simple_lineicon from './pages/icon_simple_lineicon';
import Icon_weather from './pages/icon_weather';
import Inbox from './pages/inbox';
import Message_chat from './pages/message_chat';
import Email_compose from './pages/email_compose';
import Email_details from './pages/emai_details';
import NotFound_page from './pages/404_page';
import Blank_page from './pages/Blank_page';
import Blank_page_header from './pages/Blank_page_header';
import Calendar from './pages/Calendar';
import Chart_chartist from './pages/Chart_chartist';
import Chart_charts from './pages/Chart_charts';
import Chart_gauge from './pages/Chart_gauge';
import Chart_morris from './pages/Chart_morris';
import Chart_sparkline from './pages/Chart_sparkline';
import Charts_c3 from './pages/Charts_c3';
import Color_picker from './pages/Color_picker';
import Cropper_image from './pages/Cropper_image';
import Forgot_password from './pages/Forgot_password';
import Invoice from './pages/Invoice';
import Login from './pages/Login';
import Media_object from './pages/Media_object';
import Pricing from './pages/Pricing';
import Sign_up from './pages/Sign_up';
import Sortable_nestable_list from './pages/Sortable_nestable_list';
import Timeline from './pages/Timeline';
import Widgets from './pages/Widgets';
import { Route, Switch } from 'react-router-dom';


function App() {
  return (
    <div className="App">

      <Header />
      <Sidemenu />
      <Switch>

        <Route exact path='/' component={Home} />
        <Route exact path='/dashboard_influencer' component={Dashboard_influencer} />
        <Route exact path='/dashboard_finance' component={Dashboard_finance} />
        <Route exact path='/ecommerce_product' component={Ecommerce_product} />
        <Route exact path='/ecommerce_product_single' component={Ecommerce_product_single} />
        <Route exact path='/ecommerce_product_checkout' component={Ecommerce_product_checkout} />
        <Route exact path='/influencer_finder' component={Influencer_finder} />
        <Route exact path='/influencer_profile' component={Influencer_profile} />
        <Route exact path='/dashboard_sales' component={Dashboard_sales} />
        <Route exact path='/Accordions' component={Accordions} />
        <Route exact path='/Cards' component={Cards} />
        <Route exact path='/carousel' component={Carousel} />
        <Route exact path='/general' component={General} />
        <Route exact path='/listgroup' component={Listgroup} />
        <Route exact path='/tabs' component={Tabs} />
        <Route exact path='/typography' component={Typography} />
        <Route exact path='/Form_element' component={Form_element} />
        <Route exact path='/Form_validation' component={Form_validation} />
        <Route exact path='/Multiselect' component={Multiselect} />
        <Route exact path='/data_table' component={Data_table} />
        <Route exact path='/general_table' component={General_table} />
        <Route exact path='/icon_flag' component={Icon_flag} />
        <Route exact path='/icon_fontawesome' component={Icon_fontawesome} />
        <Route exact path='/icon_material' component={Icon_material} />
        <Route exact path='/icon_themify' component={Icon_themify} />
        <Route exact path='/icon_simple_lineicon' component={Icon_simple_lineicon} />
        <Route exact path='/icon_weather' component={Icon_weather} />
        <Route exact path='/map_google' component={Map_google} />
        <Route exact path='/map_vector' component={Map_vector} />
        <Route exact path='/inbox' component={Inbox} />
        <Route exact path='/message_chat' component={Message_chat} />
        <Route exact path='/email_compose' component={Email_compose} />
        <Route exact path='/email_details' component={Email_details} />
        <Route exact path='/NotFound_page' component={NotFound_page} />
        <Route exact path='/Blank_page' component={Blank_page} />
        <Route exact path='/Blank_page_header' component={Blank_page_header} />
        <Route exact path='/Calendar' component={Calendar} />
        <Route exact path='/Chart_chartist' component={Chart_chartist} />
        <Route exact path='/Chart_charts' component={Chart_charts} />
        <Route exact path='/Chart_gauge' component={Chart_gauge} />
        <Route exact path='/Chart_morris' component={Chart_morris} />
        <Route exact path='/Chart_sparkline' component={Chart_sparkline} />
        <Route exact path='/Charts_c3' component={Charts_c3} />
        <Route exact path='/Color_picker' component={Color_picker} />
        <Route exact path='/Cropper_image' component={Cropper_image} />
        <Route exact path='/Forgot_password' component={Forgot_password} />
        <Route exact path='/Invoice' component={Invoice} />
        <Route exact path='/Login' component={Login} />
        <Route exact path='/Media_object' component={Media_object} />
        <Route exact path='/Pricing' component={Pricing} />
        <Route exact path='/Sign_up' component={Sign_up} />
        <Route exact path='/Sortable_nestable_list' component={Sortable_nestable_list} />
        <Route exact path='/Timeline' component={Timeline} />
        <Route exact path='/Widgets' component={Widgets} />

      </Switch>
      <Footer />

    </div>
  );
}

export default App;
